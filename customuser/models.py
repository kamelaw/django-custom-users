from django.db import models
# DANGER DANGER WILL ROBINSON
# always use AbstractUser until time ends
from django.contrib.auth.models import AbstractUser


# Create your models here.
class MyCustomUser(AbstractUser):
    display_name = models.CharField(max_length=60, null=True, blank=True)

    def __str__(self):
        # Jalon helped with tweaking thia
        return str(self.display_name)
