from django import forms


# saw usercreationform in django docs. i think we can't use builtin
# login but hopefully we can use this
class SignUpForm(forms.Form):
    name = forms.CharField(max_length=100)
    display_name = forms.CharField(max_length=100)
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput)


class LoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput)
