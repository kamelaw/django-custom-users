from django.shortcuts import render, HttpResponseRedirect, reverse
from django.contrib.auth import get_user_model
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from customuser.forms import SignUpForm, LoginForm
from customuser.models import MyCustomUser
# https://www.fullstackpython.com/django-conf-settings-examples.html
# Jalon helped with this
from django.conf import settings


# Create your views here.
def index(request):
    # user = request.user
    # user_data = MyCustomUser.objects.get(username=user.username)

    return render(request, 'index.html', {
        'settings': settings
    })


def signup_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            new_user = MyCustomUser.objects.create_user(
                username=data['username'],
                password=data['password'],
                display_name=data['display_name']
            )
            # Jalon had me remove this
            # MyCustomUser.objects.create(
            #     display_name=data['display_name'],
            #     user=new_user
            #
            # )
            return HttpResponseRedirect(reverse('homepage'))

    form = SignUpForm
    return render(request, 'generic_form.html', {'form': form})


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(
                request, username=data['username'], password=data['password']
            )
            if user:
                login(request, user)

                return HttpResponseRedirect(request.GET.get('next', reverse('homepage')))

    form = LoginForm
    return render(request, 'generic_form.html', {'form': form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('homepage'))
